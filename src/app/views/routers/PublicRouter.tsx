import React from 'react';
import { Route, Redirect } from 'react-router-dom';

type PublicRouterProps = {
	isAuthenticated: boolean;
	component: any;
	path: string;
	exact: boolean;
};

export const PublicRouter: React.FC<PublicRouterProps> = ({
	isAuthenticated,
	component: Component,
	path,
	exact = false,
}) => {
	return (
		<Route
			path={path}
			exact={exact}
			component={(props: any) =>
				isAuthenticated ? <Redirect to='/portal' /> : <Component {...props} />
			}
		/>
	);
};
