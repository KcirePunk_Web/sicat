export const environment = {
	baseurlSicat: 'https://sicat.app/modulos/api/webService.php',
	baseUrlAuth: 'https://isysauth.herokuapp.com/api/authdb',
	platform: 'SICAT',
	project: ['SICAT20we', 'SICAT21'],
};
