import React from 'react';
import { NavLink } from 'react-router-dom';

export const UlCoordinador = () => {
	return (
		<ul className='sidebar-menu tree' data-widget='tree'>
			<li className='nav-devider' />
			<li className='header nav-small-cap'>Coordinador</li>
			<li>
				<NavLink activeClassName='active' to='/portal/coordinador/valoracion'>
					<i className='fa fa-star icon-border-radios' />{' '}
					<span>Valoracion</span>
					<span className='pull-right-container'>
						<i className='fa fa-angle-right pull-right' />
					</span>
				</NavLink>
			</li>
			<li>
				<NavLink activeClassName='active' to='/portal/coordinador/seguimiento'>
					<i className='fa fa-star' /> <span>Seguimiento</span>
					<span className='pull-right-container'>
						<i className='fa fa-angle-right pull-right' />
					</span>
				</NavLink>
			</li>
		</ul>
	);
};
