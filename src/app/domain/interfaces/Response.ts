export interface Response {
	status: number;
	status_message: string;
	ok: boolean;
	data: User;
}
export interface User {
	super_usuario: boolean;
	archivo_imagen_perfil: string;
	estado: string;
	nivel: number;
	puntos: number;
	_id: string;
	correo: string;
	nombre_asociado: string;
	telefono_movil: string;
	autorizaciones?: AutorizacionesEntity[] | null;
	__v: number;
}
export interface AutorizacionesEntity {
	rol: string;
	privilegio: number;
	FechaDesde: string;
	FechaHasta: string;
	estado: string;
	contrato: string;
	MotivoFinalizacion?: null;
	FechaHoraFinalizacion?: null;
	modulo_predeterminado: string;
	_id: string;
	plataforma: string;
	proyecto: string;
	modulos?: ModulosEntity[] | null;
	descripcion: string;
}
export interface ModulosEntity {
	estado: string;
	fecha_inicio_vigencia: string;
	fecha_fin_vigencia: string;
	_id: string;
	id: string;
	descripcion: string;
}

export interface ResponseConfigUser {
	data: ConfigUser;
}

export interface ConfigUser {
	datosCuenta?: DatosCuentaEntity[] | null;
}
export interface DatosCuentaEntity {
	nombre_completo: string;
	email: string;
	acceso: string;
	telefono_movil: string;
	id_usuario: string;
	cuentalogin: string;
	rol_docente: boolean;
	rol_coordinador: boolean;
	rol_secretaria: boolean;
}

export interface ConfigUserDetail {
	acceso: string;
	cuentalogin: string;
	email: string;
	id_usuario: string;
	nombre_completo: string;
	rol_coordinador: boolean;
	rol_docente: boolean;
	rol_secretaria: boolean;
	telefono_movil: number;
}
