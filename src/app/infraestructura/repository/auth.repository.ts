import { AuthService } from '../http/auth.service';
import { Response } from './../../domain/interfaces/Response';

export class AuthRepository {
	private _http = AuthService.auth;
	private static _instance: AuthRepository;

	private constructor() {}

	public static get auth(): AuthRepository {
		if (!AuthRepository._instance) {
			AuthRepository._instance = new AuthRepository();
		}

		return AuthRepository._instance;
	}

	verifyEmail(email: string) {
		return this._http.get<Response>(`/cuentas/list/correo/${email}`);
	}

	signIn(email: string, password: string) {
		return this._http.post<Response>('/cuenta/verify', {
			correo: email,
			clave: password,
		});
	}
}
