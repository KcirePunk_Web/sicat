import React from 'react';
import './loading.css';

export const LoadingScreen = React.memo(() => {
	return (
		<>
			<div className='content-loading'>
				<div className='loading'>
					<p>Cargando...</p>
					<span />
				</div>
			</div>
		</>
	);
});
