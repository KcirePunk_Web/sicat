import React, { useState } from 'react';
import image1 from './images/1.jpg';
import image2 from './images/2.jpg';
import image3 from './images/3.jpg';

export const ContentCarousel = () => {
	const [titulo] = useState('SICAT 20we');
	const [
		descMessage,
	] = useState(`Trabajamos para habilitar mejoras en el sistema de información, 
    pronto tendremos un acceso unificado para facilitar el trabajo de la comunidad educativa.`);

	return (
		<div className='main-content'>
			<h3 className='title'>{titulo}</h3>
			<p className='text-justify'>{descMessage}</p>
			<br />
			<div
				id='carouselExampleSlidesOnly'
				className='carousel slide'
				data-ride='carousel'>
				<div className='carousel-inner'>
					<div className='carousel-item active'>
						<img
							src={image1}
							className='d-block max-image w-100'
							alt='Plataforma Sicat'
						/>
					</div>
					<div className='carousel-item'>
						<img
							src={image2}
							className='d-block max-image w-100'
							alt='Plataforma Sicat'
						/>
					</div>
					<div className='carousel-item'>
						<img
							src={image3}
							className='d-block max-image w-100'
							alt='Plataforma Sicat'
						/>
					</div>
				</div>
			</div>
		</div>
	);
};
