import React from 'react';
import { LoginForm } from './LoginForm';
import { ContentCarousel } from '../../../shared/content/ContentCarousel';
import logo from './logo.png';
import './login.css';

export const Login = () => {
	return (
		<div className='main-area'>
			<div className='container full-height position-static'>
				<section className='left-section full-height'>
					<a className='logo'>
						<img src={logo} alt='Logo' />
					</a>
					<div className='display-table'>
						<div className='display-table-cell'>
							<ContentCarousel />
						</div>
					</div>
					<ul className='footer-icons'>
						<li className='social'>Síguenos en: </li>
						<li className='social'>
							<a href='https://www.facebook.com/ingenieriaisys'>
								<i className='fab fa-facebook-f'></i>
							</a>
						</li>
						<li className='social'>
							<a href='https://twitter.com/ingenieriaisys'>
								<i className='fab fa-twitter'></i>
							</a>
						</li>
						<li className='social'>
							<a href='https://www.youtube.com/channel/UCMsQ3qV-Xqnr47ynbMU2xEA'>
								<i className='fab fa-youtube'></i>
							</a>
						</li>
					</ul>
				</section>
				<section className='right-section'>
					<div className='display-table center-text'>
						<div className='display-table-cell'>
							<LoginForm />
						</div>
					</div>
				</section>
			</div>
		</div>
	);
};
