import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Login } from '../pages/auth/login/Login';

export const AuthRouter = () => {
	return (
		<div>
			<Switch>
				<Route path='/auth/login' component={Login} />
			</Switch>
		</div>
	);
};
