import { AcionInitialState } from '../../domain/interfaces/acionInitialState';
import { cargoTypes } from '../../domain/types/cargo.types';

export type stateCargo = 'coordinador' | 'secretaria';

const initialState: stateCargo = 'secretaria';

export const cargoReducer = (
	state = initialState,
	action: AcionInitialState
) => {
	switch (action.type) {
		case cargoTypes.setCargo:
			return action.payload.cargo;
		default:
			return state;
	}
};
