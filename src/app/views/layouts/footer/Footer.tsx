import React from 'react';

export const Footer: React.FC = () => {
	return (
		<footer className='main-footer'>
			<div className='pull-right d-none d-sm-inline-block'>
				<ul className='nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end'>
					<li className='nav-item'>
						<a className='nav-link' />
					</li>
					<li className='nav-item'>
						<a className='nav-link' href='#'>
							elarca@SICAT20we|renov1arca2@gmail.com
						</a>
					</li>
				</ul>
			</div>
			© 2017 <a href='#'>Multi-Purpose Themes</a>. All Rights Reserved.
		</footer>
	);
};
