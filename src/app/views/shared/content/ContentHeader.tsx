import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stateCargo } from '../../../application/reducers/cargoReducer';
import { setCargo as setCargoState } from '../../../application/actions/cargo.actions';
import { Capitalize } from '../../../application/lib/helpers';
import { AppState } from '../../../application/store/store';
import { ConfigUserDetail } from '../../../domain/interfaces/Response';
import { existingRoles } from '../../../domain/types/existing-roles.types';

export const ContentHeader: React.FC = () => {
	const cargoState: any = useSelector<AppState>((state) => state.cargo);
	const configUser: any = useSelector<AppState>((state) => state.configUser);

	const [cargo, setCargo] = useState<stateCargo>(cargoState);
	const [cargos, setCargos] = useState<string[]>([]);
	const dispatch = useDispatch();

	const handleClickCargo = (
		e: React.MouseEvent<HTMLAnchorElement, MouseEvent> | any
	) => {
		const value = e.target.outerText;
		setCargo(value.toLowerCase());
		dispatch(setCargoState(value.toLowerCase()));
	};

	const memoCapitalize = useMemo(() => Capitalize(cargo), [cargo]);

	const creatingRolesPermitted = () => {
		(configUser as ConfigUserDetail[]).map((infoUser) => {
			if (infoUser.rol_coordinador) {
				setCargos([...cargos, existingRoles.coordinador]);
			} else if (infoUser.rol_docente) {
				setCargos([...cargos, existingRoles.docente]);
			} else if (infoUser.rol_secretaria) {
				setCargos([...cargos, existingRoles.secretaria]);
			}
		});
	};

	useEffect(() => {
		creatingRolesPermitted();
	}, []);

	return (
		<section className='content-header'>
			<div className='col-md-3 col-12'>
				<div style={{ margin: 0 }} className='box'>
					<div className='box-header with-border'>
						<h4 style={{ color: '#000000' }} className='box-title mt-4'>
							{memoCapitalize}
						</h4>
						<ul className='box-controls pull-right ajustar'>
							<li className='dropdown'>
								<a data-toggle='dropdown' href='#' aria-expanded='false'>
									<i className='fas fa-arrow-right'></i>
								</a>
								<div
									className='dropdown-menu dropdown-menu-right'
									x-placement='bottom-end'
									style={{
										position: 'absolute',
										transform: 'translateY(20px)',
										top: 0,
										left: 0,
										willChange: 'transform',
									}}>
									{cargos.map((cargo) => (
										<a
											key={cargo}
											className='dropdown-item rol pointer'
											onClick={handleClickCargo}>
											{Capitalize(cargo)}
										</a>
									))}
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
	);
};
