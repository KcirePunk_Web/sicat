import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { authReducer, authState } from '../reducers/authReducer';
import { cargoReducer, stateCargo } from '../reducers/cargoReducer';
import {
	accountSettingsReducer,
	accountSettingsState,
} from '../reducers/account-settingsReducer';

const composeEnhancers =
	(window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export interface AppState {
	auth: authState;
	cargo: stateCargo;
	configUser: accountSettingsState;
}

const reducers = combineReducers({
	auth: authReducer,
	cargo: cargoReducer,
	configUser: accountSettingsReducer,
});

export const store = createStore(
	reducers,
	composeEnhancers(applyMiddleware(thunk))
);
