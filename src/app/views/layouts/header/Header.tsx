import React from 'react';
import { useSelector } from 'react-redux';
import { AppState } from '../../../application/store/store';
import { User } from '../../../domain/interfaces/Response';

export const Header = () => {
	const { auth }: any = useSelector<AppState>((state) => state.auth);

	const user: User = auth;

	return (
		<header className='main-header'>
			{/* Header Navbar: style can be found in header.less */}
			<nav className='navbar navbar-static-top color'>
				{/* Sidebar toggle button*/}
				<a
					href='#'
					className='sidebar-toggle'
					data-toggle='push-menu'
					role='button'>
					<i className='fas fa-bars' style={{ fontSize: '16px' }}></i>
					{/* <span className='sr-only'>Toggle navigation</span> */}
				</a>
				<div className='navbar-custom-menu'>
					<ul className='nav navbar-nav'>
						<li className='dropdown user user-menu nombreUser'>
							<a href='#' style={{ fontSize: 12 }} className='dropdown-toggle'>
								{user.nombre_asociado}
							</a>
						</li>
						{/* User Account: style can be found in dropdown.less */}
						<li className='dropdown user user-menu'>
							<a href='#' className='dropdown-toggle' data-toggle='dropdown'>
								<img
									src='/colegioenlinea/SICAT2.1/portal/images/elarca.png'
									className='user-image imgColegio rounded-circle'
									alt='User Image'
								/>
							</a>
							<ul className='dropdown-menu scale-up'>
								{/* User image */}
								<li className='user-header'>
									<img
										style={{ width: 90 }}
										src='/colegioenlinea/SICAT2.1/portal/images/elarca.png'
										className=' imgColegio float-left rounded-circle'
										alt='User Image'
									/>
									<p style={{ fontSize: 13 }}>
										NANCY CHAVERRA PALACIO
										<small className='mb-5'>renov1arca2@gmail.com</small>
									</p>
								</li>
								{/* Menu Body */}
								<li className='user-body'>
									<div className='row no-gutters'>
										<div role='separator' className='divider col-12' />
										<div className='col-12 text-left'>
											<a href='/colegioenlinea/SICAT2.1/portal/cerrarSesion.php'>
												<i className='fa fa-power-off' /> Logout
											</a>
										</div>
									</div>
									{/* /.row */}
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
	);
};
