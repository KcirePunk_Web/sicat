

$(function () {
    
	$.each(datosWS,function(i ,item){
		$('#example1').DataTable({
		    "ajax": item.configuracion.Url_Listar_Entidad,
		    "responsive": true,
		    
		    createdRow: function( row, data, dataIndex ) {
		        var id = $(row).find('td:eq(0)').html();
		        var referencia = $(row).find('td:eq(1)').html();
		        var movil = $(row).find('td:eq(2)').html();
		        var nmro_id = $(row).find('td:eq(3)').html();
		        $(row).attr('data-id', id);
		        $(row).attr('data-referencia', referencia);
		        $(row).attr('data-movil', movil);
		        $(row).attr('data-numroId', nmro_id);
		    },
		    "language":idioma_espanol
		});
		$('#example1 tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
                
            }
            else {
                $('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                var id = $(this).attr("data-id");
                var referencia = $(this).attr("data-referencia");
                var nmro_id = $(this).attr("data-numroId");
                 var movil = $(this).attr("data-movil");
                 $.each(datosWS,function(i ,item){
                    $.post( item.configuracion.url_insert_itemFact, { movil:movil,id_item_fact:id,identificacion:nmro_id,referencia:referencia }, function(respuesta) {
                        if (respuesta.data == true) {
                            mostrar_detalles(id);
                            $("#navDetalles").slideDown();
                        }
                    }, "json");
                 }) 
            }
        });
	})
}); // End of use strict

var idioma_espanol={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}