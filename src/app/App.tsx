import React from 'react';
import { Provider } from 'react-redux';
import { AppRouter } from './views/routers/AppRouter';
import { store } from './application/store/store';
import './styles.css';

function App() {
	return (
		<Provider store={store}>
			<div className='wrapper'>
				<AppRouter />
			</div>
		</Provider>
	);
}

{
	/* <input id='busqueda_ruta' style={{ display: 'none' }} defaultValue='si' /> */
}

// function App() {
// 	return (
// 		<div className='wrapper'>
// 			<Header />
// 			<Sidebar />
// 			<ContentWrapper>
// 				<ContentHeader />

// 				{/* Main content */}
// 				<section id='formaValoracion' className='content'>
// 					<div className='box'>
// 						<div className='box-header with-border'>
// 							<h3 className='box-title'>Valoración</h3>
// 							<div className=' pull-right'>
// 								<button
// 									type='button'
// 									className='btn btn-box-tool'
// 									data-widget='collapse'>
// 									<i className='fa fa-minus' />
// 								</button>
// 							</div>
// 						</div>
// 						{/* /.box-header */}
// 						<div className='box-body'>
// 							<div className='row'>
// 								<div className='col-md-4 col-12'>
// 									{/* /.form-group */}
// 									<div className='form-group'>
// 										<label>Año</label>
// 										<select id='año' className='form-control select2 w-p100'>
// 											<option value={2017}>Selecione un año</option>
// 											<option value={2017}>2017</option>
// 											<option value={2017}>2018</option>
// 										</select>
// 									</div>
// 								</div>
// 								{/* /.col */}
// 								<div className='col-md-4 col-12'>
// 									{/* /.form-group */}
// 									<div className='form-group'>
// 										<label>Periodo</label>
// 										<select
// 											id='periodo'
// 											className='form-control select2 w-p100'>
// 											<option value='selected'>Selecione un periodo</option>
// 											<option value={2}>2</option>
// 										</select>
// 									</div>
// 								</div>
// 								<div className='col-md-4 col-12'>
// 									{/* /.form-group */}
// 									<div className='form-group'>
// 										<label>Curso</label>
// 										<select
// 											id='listCurso'
// 											className='form-control select2 w-p100'></select>
// 									</div>
// 								</div>
// 								{/* /.col */}
// 							</div>
// 							<div className='text-xs-right'>
// 								<button type='button' id='Valorar' className='btn btn-info'>
// 									Valorar
// 								</button>
// 							</div>
// 							{/* /.row */}
// 						</div>
// 						{/* /.box-body */}
// 					</div>
// 				</section>
// 			</ContentWrapper>
// 			<Footer />
// 		</div>
// 	);
// }

export default App;
