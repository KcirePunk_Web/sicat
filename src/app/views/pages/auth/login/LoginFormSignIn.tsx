import React from 'react';
import { useDispatch } from 'react-redux';
import { Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { AlertMessage } from '../../../components/AlertMessage/AlertMessage';
import { AuthRepository } from '../../../../infraestructura/repository/auth.repository';
import { Loading } from '../../../components/LoadingSpinner/Loading';
import { login } from '../../../../application/actions/auth.actions';

type LoginFormSignInProps = {
	isValidEmail: boolean;
	email: string;
};

type FormValues = {
	clave: string;
	terms: boolean;
};

const initialValues: FormValues = {
	clave: '',
	terms: false,
};

const validationSchemaSignIn = Yup.object({
	terms: Yup.boolean().isTrue(),
	clave: Yup.string().required('La contraseña no es valida'),
});

export const LoginFormSignIn: React.FC<LoginFormSignInProps> = ({ email }) => {
	const dispatch = useDispatch();

	const handleSignIn = async (
		{ clave }: FormValues,
		{ setErrors }: FormikHelpers<FormValues>
	) => {
		try {
			const { data } = await AuthRepository.auth.signIn(email, clave);

			if (!data.ok) {
				setErrors({
					clave: 'Contraseña incorrecta',
				});
			}

			dispatch(login(data.data, true, {}));
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<Formik
			onSubmit={handleSignIn}
			initialValues={initialValues}
			validationSchema={validationSchemaSignIn}>
			{({
				values,
				handleSubmit,
				handleChange,
				handleBlur,
				errors,
				touched,
				isSubmitting,
			}) => (
				<form className='form-group' onSubmit={handleSubmit}>
					<input
						type='password'
						name='clave'
						value={values.clave}
						onChange={handleChange}
						onBlur={handleBlur}
						placeholder='Clave'
					/>
					{errors.clave && touched.clave && (
						<AlertMessage
							className='wi-85'
							text={errors.clave}
							background='danger'
						/>
					)}
					<button type='submit'>
						<span>Iniciar Sesion</span>
						{isSubmitting && <Loading type='spin' color='#fff' />}
					</button>

					<div className='p-3'>
						{/* <p>
							Como usuario registrado en la plataforma SIGETT reconozco que debo
							dar un tratamiento de veracidad, cuidado y alta confidencialidad a
							los datos, a razón de lo cual, autorizo a la Empresa de Tránsito y
							Transporte de Bolívar a realizar los seguimientos y auditorias que
							esta considere pertinente.
						</p> */}
						<input
							value={String(values.terms)}
							onChange={handleChange}
							type='checkbox'
							id='terms'
							name='terms'
						/>
						<label className='mt-3' htmlFor='terms'>
							<span>Acepto los terminos</span>
						</label>

						{errors.terms && touched.terms && (
							<AlertMessage
								className='wi-85'
								text='Debe aceptar los terminos y condiciones'
								background='danger'
							/>
						)}
					</div>
				</form>
			)}
		</Formik>
	);
};
