import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { identifyRole } from '../../application/actions/account-settings.actions';
import { SettingsRepository } from '../../infraestructura/repository/settings.repository';
import { AppState } from '../../application/store/store';
import { LoadingScreen } from '../components/LoadingSpinner/LoadingScreen';

type TesterProps = {
	setIsChecking: any;
};

// sneiderllain@gmail.com fnt076ef

export const Tester: React.FC<TesterProps> = ({ setIsChecking }) => {
	const dispatch = useDispatch();
	const user: any = useSelector<AppState>((state) => state.auth);

	const getInfoUser = () => {
		SettingsRepository.service
			.getIdentifyRoleUser(user.correo)
			.then((resp) => {
				console.log(resp.data.data.datosCuenta);

				dispatch(identifyRole(resp.data.data.datosCuenta));

				setIsChecking(false);
			})
			.catch(() => {
				// dispatch(identifyRole({}));
				setIsChecking(false);
			});
	};

	useEffect(() => {
		getInfoUser();
	}, []);

	return <LoadingScreen />;
};
