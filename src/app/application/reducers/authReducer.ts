import { authTypes } from '../../domain/types/auth.types';
import { AcionInitialState } from '../../domain/interfaces/acionInitialState';

export type authState = {
	isAuth: boolean;
	auth: {};
	configUser: {};
};

const initialState: authState = {
	isAuth: false,
	auth: {},
	configUser: {},
};

export const authReducer = (
	state = initialState,
	action: AcionInitialState
) => {
	switch (action.type) {
		case authTypes.login:
			return {
				isAuth: action.payload.isAuth,
				auth: action.payload.user,
				userConfig: action.payload.userConfig,
			};
		case authTypes.logout:
			return {};
		default:
			return state;
	}
};
