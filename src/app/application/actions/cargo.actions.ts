import { cargoTypes } from '../../domain/types/cargo.types';

export const setCargo = (cargo: string) => ({
	type: cargoTypes.setCargo,
	payload: {
		cargo,
	},
});
