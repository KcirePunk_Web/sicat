import { authTypes } from '../../domain/types/auth.types';

export const login = (user: any, isAuth: boolean, userConfig: any) => ({
	type: authTypes.login,
	payload: {
		user,
		isAuth,
		userConfig,
	},
});
