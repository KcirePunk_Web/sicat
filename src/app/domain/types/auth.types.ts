export enum authTypes {
	login = '[Auth] Login',
	logout = '[Auth] Logout',
}
