import React from 'react';

type AlertMessageProps = {
	text: string;
	background?: backgroundColor;
	className?: string;
};

type backgroundColor =
	| 'primary'
	| 'secondary'
	| 'success'
	| 'danger'
	| 'warning'
	| 'info'
	| 'light'
	| 'dark';

export const AlertMessage: React.FC<AlertMessageProps> = React.memo(
	({ text = '', background = 'primary', className = '' }) => {
		return (
			<>
				{text !== '' && (
					<div
						className={`alert alert-${background} ${className}`}
						role='alert'>
						{text}
					</div>
				)}
			</>
		);
	}
);
