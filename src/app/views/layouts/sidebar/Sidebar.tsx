import React from 'react';
import { useSelector } from 'react-redux';
import { AppState } from '../../../application/store/store';
import { UlCoordinador } from './UlCoordinador';
import { UlSecretaria } from './UlSecretaria';

// 'coordinador' | 'secretaria'

export const Sidebar: React.FC = () => {
	const cargo = useSelector<AppState>((state) => state.cargo);
	return (
		<>
			{/* Left side column. contains the logo and sidebar */}
			<aside className='main-sidebar'>
				{/* sidebar: style can be found in sidebar.less */}
				<section className='sidebar' style={{ height: 'auto' }}>
					{/* Sidebar user panel */}
					<div className='user-panel'>
						<div className='ulogo'>
							<a href='#'>
								{/* logo for regular state and mobile devices */}
								<span>
									<b>Panel </b>Admin
								</span>
							</a>
						</div>
						<div className='image'>
							<img
								src='/colegioenlinea/SICAT2.1/portal/images/SICAT.png'
								className='rounded-circle logoProyecto'
								alt='User Image'
							/>
						</div>
						<div className='info'>
							<p className='nombreCliente' />
							<a
								href='/colegioenlinea/SICAT2.1/portal/cerrarSesion.php'
								className='link'
								data-toggle='tooltip'
								data-original-title='Logout'>
								<i className='fas fa-power-off'></i>
							</a>
						</div>
					</div>
					{/* sidebar menu: : style can be found in sidebar.less */}
					{cargo === 'secretaria' ? <UlSecretaria /> : <UlCoordinador />}
				</section>
			</aside>
		</>
	);
};
