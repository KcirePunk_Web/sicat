import { environment } from './../../../environment/environment';
import { HttpService } from './http.service';

export class SicatService extends HttpService {
	private static _instance: SicatService;

	private constructor() {
		super(environment.baseurlSicat);
	}

	public static get http(): SicatService {
		if (!SicatService._instance) {
			SicatService._instance = new SicatService();
		}

		return SicatService._instance;
	}
}
