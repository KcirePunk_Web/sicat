const path = require('path');

module.exports = function override(config) {
	config.resolve = {
		...config.resolve,
		alias: {
			...config.alias,
			'@environment': path.resolve(__dirname, './src/environment'),
			'@application': path.resolve(__dirname, './src/app/application'),
			'@domain': path.resolve(__dirname, './src/app/domain/*'),
			'@infraestructura': path.resolve(
				__dirname,
				'./src/app/infraestructura/*'
			),
			'@views': path.resolve(__dirname, './src/app/views/*'),
		},
	};
	return config;
};
