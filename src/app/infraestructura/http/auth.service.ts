import { environment } from './../../../environment/environment';
import { HttpService } from './http.service';
export class AuthService extends HttpService {
	private static _instance: AuthService;

	private constructor() {
		super(environment.baseUrlAuth);
	}

	public static get auth(): HttpService {
		if (!AuthService._instance) {
			AuthService._instance = new AuthService();
		}
		return AuthService._instance;
	}
}
