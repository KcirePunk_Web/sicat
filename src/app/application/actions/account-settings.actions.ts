import { accountSettingsTypes } from '../../domain/types/account.settings.types';

export const identifyRole = (rolUser: any) => ({
	type: accountSettingsTypes.identifyRole,
	payload: rolUser,
});
