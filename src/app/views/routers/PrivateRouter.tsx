import React from 'react';

import { Route, Redirect } from 'react-router-dom';

type PrivateRouterProps = {
	isAuthenticated: boolean;
	component: any;
	path: string;
	exact: boolean;
};

export const PrivateRouter: React.FC<PrivateRouterProps> = ({
	isAuthenticated,
	component: Component,
	path,
	exact = false,
}) => {
	return (
		<Route
			path={path}
			exact={exact}
			component={(props: any) =>
				isAuthenticated ? (
					<Component {...props} />
				) : (
					<Redirect to='/auth/login' />
				)
			}
		/>
	);
};
