import React from 'react';
import ReactLoading from 'react-loading';

type LoadingPros = {
	type?: spinners;
	color?: string;
};

type spinners =
	| 'blank'
	| 'balls'
	| 'bars'
	| 'bubbles'
	| 'cubes'
	| 'cylon'
	| 'spin'
	| 'spinningBubbles'
	| 'spokes';

export const Loading: React.FC<LoadingPros> = React.memo(
	({ type = 'spin', color = '#fff' }) => {
		return <ReactLoading type={type} color={color} className='small-spinner' />;
	}
);
