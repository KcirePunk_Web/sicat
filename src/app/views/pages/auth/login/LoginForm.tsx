import React, { useState } from 'react';
import { User } from '../../../../domain/interfaces/Response';
import { LoginFormEmail } from './LoginFormEmail';
import { LoginFormSignIn } from './LoginFormSignIn';

export const LoginForm = () => {
	const [isValidEmail, setIsValidEmail] = useState<boolean>(false);
	const [infoUser, setInfoUser] = useState<Partial<User>>({});

	return (
		<div id='login'>
			<div className='wrapper fadeInDown'>
				<div id='formContent'>
					<div className='fadeIn first'>
						<h3 className='p-3 font-bold'>Autenticación al sistema</h3>
						<i className='far fa-user pt-3' style={{ fontSize: 50 }}></i>{' '}
						<h4 style={{ display: 'inline-block' }}>
							{infoUser.nombre_asociado}
						</h4>
					</div>

					<LoginFormEmail
						setIsValidEmail={setIsValidEmail}
						isValidEmail={isValidEmail}
						setInfoUser={setInfoUser}
					/>

					{isValidEmail && (
						<LoginFormSignIn
							email={infoUser.correo as string}
							isValidEmail={isValidEmail}
						/>
					)}
					{/* isValidEmail={isValidEmail} */}

					<div id='formFooter'>
						<span className='underlineHover'>Olvidaste la contraseña?</span>
					</div>
				</div>
			</div>
		</div>
	);
};
