import React, { useState } from 'react';
import { Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';
import { AlertMessage } from '../../../components/AlertMessage/AlertMessage';
import { AuthRepository } from '../../../../infraestructura/repository/auth.repository';
import { Loading } from '../../../components/LoadingSpinner/Loading';
import { User } from '../../../../domain/interfaces/Response';
import { environment } from '../../../../../environment/environment';

type LoginFormEmailProps = {
	isValidEmail: boolean;
	setIsValidEmail: React.Dispatch<React.SetStateAction<boolean>>;
	setInfoUser: React.Dispatch<React.SetStateAction<Partial<User>>>;
};

type FormValues = {
	correo: string;
};

const initialValues = {
	correo: '',
};

const validationSchemaEmail = Yup.object({
	correo: Yup.string().email('No es valido').required('No es valido'),
});

export const LoginFormEmail: React.FC<LoginFormEmailProps> = ({
	isValidEmail,
	setIsValidEmail,
	setInfoUser,
}) => {
	const [plataform] = useState(environment.platform);
	const [project] = useState(environment.project);

	const handleVerifyEmail = async (
		{ correo }: FormValues,
		{ setErrors }: FormikHelpers<FormValues>
	) => {
		try {
			const { data } = await AuthRepository.auth.verifyEmail(correo);

			if (!data.ok) {
				setErrors({
					correo: 'El correo no esta registrado en el sistema',
				});
			} else {
				const isVerify = checkPlatformAndProject(data.data.autorizaciones);

				if (isVerify) {
					setIsValidEmail(true);
					setInfoUser(data.data);
				} else {
					setErrors({
						correo: 'Permino denegado',
					});
				}
			}
		} catch (error) {
			console.log(error);
		}
	};

	const checkPlatformAndProject = (autorizaciones: any) => {
		const userData = autorizaciones.find(
			(autorizacion: any) =>
				project.includes(autorizacion.proyecto) &&
				autorizacion.estado === 'A' &&
				autorizacion.plataforma === plataform
		);

		return userData !== undefined ? true : false;
	};

	return (
		<Formik
			onSubmit={handleVerifyEmail}
			initialValues={initialValues}
			validationSchema={validationSchemaEmail}>
			{({
				values,
				handleSubmit,
				handleChange,
				handleBlur,
				touched,
				errors,
				isSubmitting,
			}) => (
				<form onSubmit={handleSubmit}>
					<div className='form-group mt-3'>
						<input
							type='text'
							className='fadeIn second wi-100'
							name='correo'
							readOnly={isValidEmail}
							value={values.correo}
							autoComplete='off'
							onChange={handleChange}
							onBlur={handleBlur}
							placeholder='Correo Electronico'
						/>
						{errors.correo?.includes('No es valido') && touched.correo && (
							<AlertMessage
								className='wi-85'
								text='El correo no es valido'
								background='danger'
							/>
						)}
						{errors.correo?.includes('no esta registrado') && !isValidEmail && (
							<AlertMessage
								text='El correo no esta registrado en el sistema'
								background='danger'
								className='wi-85'
							/>
						)}
						{errors.correo?.includes('Permino denegado') && !isValidEmail && (
							<AlertMessage
								text='Usted no tiene permiso para ingresar'
								background='info'
								className='wi-85'
							/>
						)}

						{!isValidEmail && (
							<button
								disabled={isSubmitting}
								type='submit'
								className='fadeIn fourth'>
								<span className='mb-2'>Validar Acesso</span>{' '}
								{isSubmitting && <Loading type='spin' color='#fff' />}
							</button>
						)}
					</div>
				</form>
			)}
		</Formik>
	);
};
