import { ResponseConfigUser } from './../../domain/interfaces/Response';
import { SicatService } from './../http/sicat.service';

export class SettingsRepository {
	private static _instance: SettingsRepository;

	private _http: SicatService = SicatService.http;

	public static get service(): SettingsRepository {
		if (!SettingsRepository._instance) {
			SettingsRepository._instance = new SettingsRepository();
		}

		return SettingsRepository._instance;
	}

	getIdentifyRoleUser(email: string) {
		return this._http.get<ResponseConfigUser>('', {
			params: {
				apikey: 'elarca@SICAT20we',
				metodo: 'recuperarCuentaUsuario',
				pmtos: 'erick792efta@gmail.com',
			},
		});
	}
}
