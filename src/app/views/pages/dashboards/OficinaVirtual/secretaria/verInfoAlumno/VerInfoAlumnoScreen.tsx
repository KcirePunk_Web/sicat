import React from 'react';

export const VerInfoAlumnoScreen: React.FC = () => {
	return (
		<section className='content dashboard'>
			<div className='row'>
				<div className='cursor col-xl-4 col-md-6 col-12'>
					<div className='info-box'>
						<span className='info-box-icon bg-blue'>
							<i className='fas fa-users'></i>
						</span>
						<div className='info-box-content'>
							<span className='info-box-number'>Responsables / Acudientes</span>
						</div>
						{/* /.info-box-content */}
					</div>
					{/* /.info-box */}
				</div>
				<div className='cursor col-xl-4 col-md-6 col-12'>
					<div className='info-box'>
						<span className='info-box-icon bg-blue'>
							<i className='fas fa-edit'></i>
						</span>
						<div className='info-box-content'>
							<span className='info-box-number'>Edición de Matrícula</span>
						</div>
						{/* /.info-box-content */}
					</div>
					{/* /.info-box */}
				</div>
				<div className='col-xl-4 col-md-6 col-12'>
					<div className='info-box'>
						<span className='info-box-icon bg-blue'>
							<i className='fas fa-file-medical'></i>
						</span>
						<div className='info-box-content'>
							<span className='info-box-number'>Generar Hoja Matrícula</span>
						</div>
						{/* /.info-box-content */}
					</div>
					{/* /.info-box */}
				</div>
				<div className='col-xl-4 col-md-6 col-12'>
					<div className='info-box'>
						<span className='info-box-icon bg-blue'>
							<i className='fas fa-certificate'></i>
						</span>
						<div className='info-box-content'>
							<span className='info-box-number'>Certificado Notas</span>
						</div>
						{/* /.info-box-content */}
					</div>
					{/* /.info-box */}
				</div>
				<div className=' DashboardArea Compartir col-xl-4 col-md-6 col-12'>
					<div className='info-box'>
						<span className='info-box-icon bg-blue'>
							<i className='fas fa-user-check'></i>
						</span>
						<div className='info-box-content'>
							<span className='info-box-number'>Constancia Estudio</span>
						</div>
						{/* /.info-box-content */}
					</div>
					{/* /.info-box */}
				</div>
				<div className='col-xl-4 col-md-6 col-12'>
					<div className='info-box'>
						<span className='info-box-icon bg-blue'>
							<i className='fas fa-paste'></i>
						</span>
						<div className='info-box-content'>
							<span className='info-box-number'>Copia de Boletín</span>
						</div>
						{/* /.info-box-content */}
					</div>
					{/* /.info-box */}
				</div>
			</div>
			<div className='pdf row'></div>
		</section>
	);
};
