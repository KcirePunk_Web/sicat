import React from 'react';

export const SeguimientoScreen = () => {
	return (
		<section id='formSeguimiento' className='content'>
			<div className='box'>
				<div className='box-header with-border'>
					<h3 className='box-title'>Seguimiento</h3>
					<div className=' pull-right'>
						<button
							type='button'
							className='btn btn-box-tool'
							data-widget='collapse'>
							<i className='fa fa-minus' />
						</button>
					</div>
				</div>
				{/* /.box-header */}
				<div className='box-body'>
					<div className='row'>
						<div className='col-md-4 col-12'>
							{/* /.form-group */}
							<div className='form-group'>
								<label>Año</label>
								<select id='año' className='form-control select2 w-p100'>
									<option value='selected'>Selecione un año</option>
									<option value={2017}>2017</option>
									<option value={2017}>2018</option>
								</select>
							</div>
						</div>
						{/* /.col */}
						<div className='col-md-4 col-12'>
							{/* /.form-group */}
							<div className='form-group'>
								<label>Periodo</label>
								<select id='periodo' className='form-control select2 w-p100'>
									<option value='selected'>Selecione un periodo</option>
									<option value={2}>2</option>
								</select>
							</div>
						</div>
						<div className='col-md-4 col-12'>
							{/* /.form-group */}
							<div className='form-group'>
								<label>Grado</label>
								<select id='listGrado' className='form-control select2 w-p100'>
									<option value='selected'>Selecione un grado</option>
									<option value={4}>CUARTO</option>
									<option value={5}>QUINTO</option>
									<option value={6}>SEXTO</option>
									<option value={7}>SEPTIMO</option>
								</select>
							</div>
						</div>
						{/* /.col */}
					</div>
					<div className='text-xs-right'>
						<button type='button' id='consultar' className='btn btn-info'>
							Consultar
						</button>
					</div>
					{/* /.row */}
				</div>
				{/* /.box-body */}
			</div>
		</section>
	);
};
