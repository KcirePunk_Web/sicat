import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Header } from '../layouts/header/Header';
import { Sidebar } from '../layouts/sidebar/Sidebar';
import { ContentWrapper } from '../shared/content/ContentWrapper';
import { ContentHeader } from '../shared/content/ContentHeader';
import { Footer } from '../layouts/footer/Footer';
import { ValoracionSecretariaScreen } from '../pages/dashboards/OficinaVirtual/secretaria/valoracion/ValoracionSecretariaScreen';
import { VerInfoAlumnoScreen } from '../pages/dashboards/OficinaVirtual/secretaria/verInfoAlumno/VerInfoAlumnoScreen';
import { ValoracionCoordinadorScreen } from '../pages/dashboards/OficinaVirtual/coordinador/valoracion/ValoracionCoordinadorScreen';
import { SeguimientoScreen } from '../pages/dashboards/OficinaVirtual/coordinador/seguimiento/SeguimientoScreen';
import { Tester } from '../pages/Tester';
import { useSelector } from 'react-redux';
import { AppState } from '../../application/store/store';
import { ConfigUserDetail } from '../../domain/interfaces/Response';

export const PortalRouter = () => {
	const [isChecking, setIsChecking] = useState(true);
	const configUser: any = useSelector<AppState>((state) => state.configUser);

	if ((configUser as ConfigUserDetail[]).length === 0) {
		if (isChecking) {
			return <Tester setIsChecking={setIsChecking} />;
		}
	}
	return (
		<>
			<Header />
			<Sidebar />

			<ContentWrapper>
				<ContentHeader />
				<Switch>
					<Route
						path='/portal/secretaria/valoracion'
						component={ValoracionSecretariaScreen}
					/>
					<Route
						path='/portal/secretaria/ver-info-alumno'
						component={VerInfoAlumnoScreen}
					/>

					<Route
						path='/portal/coordinador/valoracion'
						component={ValoracionCoordinadorScreen}
					/>

					<Route
						path='/portal/coordinador/seguimiento'
						component={SeguimientoScreen}
					/>
				</Switch>
			</ContentWrapper>

			<Footer />
		</>
	);
};
