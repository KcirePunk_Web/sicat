import React from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter as Router, Switch, Redirect } from 'react-router-dom';
import { authState } from '../../application/reducers/authReducer';
import { AppState } from '../../application/store/store';
import { AuthRouter } from './AuthRouter';
import { PortalRouter } from './PortalRouter';
import { PrivateRouter } from './PrivateRouter';
import { PublicRouter } from './PublicRouter';

export const AppRouter = () => {
	const auth: any | authState = useSelector<AppState>((state) => state.auth);

	return (
		<Router>
			<div>
				<Switch>
					<PublicRouter
						exact={false}
						path='/auth'
						isAuthenticated={auth.isAuth}
						component={AuthRouter}
					/>
					<PrivateRouter
						exact={false}
						path='/portal'
						isAuthenticated={auth.isAuth}
						component={PortalRouter}
					/>

					<Redirect to='/auth/login' />
					{/* <Route path='/auth' component={AuthRouter} />
					<Route path='/portal' component={PortalRouter} /> */}
				</Switch>
			</div>
		</Router>
	);
};
