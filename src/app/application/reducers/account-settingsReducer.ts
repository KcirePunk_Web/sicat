import { AcionInitialState } from './../../domain/interfaces/acionInitialState';
import { accountSettingsTypes } from '../../domain/types/account.settings.types';

export type accountSettingsState = [];

const initialState: accountSettingsState = [];

export const accountSettingsReducer = (
	state = initialState,
	action: AcionInitialState
) => {
	switch (action.type) {
		case accountSettingsTypes.identifyRole:
			console.log(action.payload);

			return action.payload;
		default:
			return state;
	}
};
