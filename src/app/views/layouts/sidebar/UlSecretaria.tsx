import React from 'react';
import { NavLink } from 'react-router-dom';

export const UlSecretaria = () => {
	return (
		<ul className='sidebar-menu tree' data-widget='tree'>
			<li className='nav-devider' />
			<li className='header nav-small-cap'>Secretaria</li>
			<li>
				<NavLink
					activeClassName='active'
					to='/portal/secretaria/ver-info-alumno'>
					<i className='fas fa-users icon-border-radios'></i>{' '}
					<span>Informacion de Alumnós</span>
					<span className='pull-right-container'>
						<i className='fa fa-angle-right pull-right' />
					</span>
				</NavLink>
			</li>
			<li>
				<NavLink activeClassName='active' to='/portal/secretaria/valoracion'>
					<i className='fa fa-star' /> <span>Valoración</span>
					<span className='pull-right-container'>
						<i className='fa fa-angle-right pull-right' />
					</span>
				</NavLink>
			</li>
		</ul>
	);
};
