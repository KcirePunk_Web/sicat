import React from 'react';

export const ContentWrapper: React.FC = ({ children }) => {
	return (
		<div className='content-wrapper' style={{ minHeight: '526.559px' }}>
			{children}
		</div>
	);
};
